var id = 0;


module.exports = {
  // Map of hooks
  hooks: {},

  // Map of new blocks
  blocks: {
    reactComponent: {
      process: function (block) {
        var componentName = block.args[0];
        id++;
        return (
          "<div>" +
          "<b>Need to add a rect component here..</b>" +
          "<b>The component name is: <u>" + componentName + "</u></b>" +
          "<b>The id used will be: " + id + "</b>" +
          "<div id=\"comp-" + id + "\"></div>" +
          "<script>console.log(\"GitbookReactRenderer('" + componentName + "', 'comp-" + id + "')\")</script>" +
          "</div>"
        );
      }
    }
  },

  // Map of new filters
  filters: {}
};
